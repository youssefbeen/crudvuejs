import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/api/member',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/api/member',
    method: 'post',
    data
  })
}

export function updateArticle(data, id) {
  return request({
    url: '/api/member/' + id,
    method: 'put',
    data
  })
}

export function deleteArticle(id) {
  return request({
    url: '/api/member/' + id,
    method: 'delete'
  })
}

