import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    'username': username,
    'password': password,
    'client_id': '2',
    'client_secret': process.env.CLIENT_CODE,
    'grant_type': 'password'
  }
  return request({
    url: '/oauth/token',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/login/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

